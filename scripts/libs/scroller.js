var Scroller = function(content, container, tab)
{
  this._content = $(content);
  this._container = $(container);
  this._tab = $(tab);
  this._tab_height = this._tab.height();
  this._lastY = false;

  this._scroll = false;

  var t = this;

  t._content_height = 0;
  t._container_height = 0;
  t._tab_parent_height = 0;

  this.init = function()
  {
    this._tab.mousedown(this._mousedown);
    $(document).mouseup(this._mouseup);
    $(document).mousemove(this._mousemove);
    $(window).resize(this._updateSize);
    $(window).resize(this._setScrollPos);
    this._content.bind('DOMMouseScroll', this._mouseDOMWheel);
    this._content.bind('mousewheel', this._mouseWheel)

    this._updateSize();

    //this._content.css({marginTop: -this._container_height});
    //this._setScrollPos();
  };

  this.run = function()
  {
    //this._updateSize();
    this._content.attr('style', ' ');
    this._updateSize();
    this._placeTab(2000);
    this._setScrollPos();
  };

  this.destroy = function()
  {
    this.stopAutoscroll();

    this._tab.unbind('mousedown', this._mousedown);
    $(document).unbind('mouseup', this._mouseup);
    $(document).unbind('mousemove', this._mousemove);
    $(window).unbind('resize', this._updateSize);
    $(window).unbind('resize', this._setScrollPos);
    this._content.unbind('DOMMouseScroll', this._mouseDOMWheel);
    this._content.unbind('mousewheel', this._mouseWheel)
  };

  this.startAutoscroll = function()
  {
    this.stopAutoscroll();

    if(t._container_height > 0)
      return;

    this._placeTab(2000);
    this._setScrollPos();

    this._autoscroll = setTimeout(function()
                                  {

                                    var tab_pos = t._tab.position().top;
                                    //var step = (t._container_height / 15) * (t._tab_parent_height / t._container_height);
                                    var step = tab_pos / 10;

                                    if(step > 10)
                                      step = 10;
                                    else if(step < 1)
                                      step = 1;


                                    if(!t._placeTab(t._tab.position().top - step))
                                      t._autoscroll = setTimeout(arguments.callee, 10);

                                    t._setScrollPos();

                                  }, 500);
  };

  this.stopAutoscroll = function()
  {
    if(this._autoscroll)
      clearTimeout(this._autoscroll);
  };

  this._updateSize = function()
  {
    t._content_height = t._content.height();
    t._container_height = t._container.height() - t._content_height;
    t._tab_parent_height = t._tab.parent().height() - t._tab_height;
  };

  this._setScrollPos = function()
  {

    if(t._container_height > 0)
      {
        t._content.css({marginTop: 0});
        t.hideTab();
      }
      else
        {
          var tab_pos = t._tab.position().top;

          if (tab_pos > t._tab_parent_height) 
            {
              t._placeTab(tab_pos);
              tab_pos = t._tab.position().top;
            }

            t.showTab();

            t._content.css({marginTop: (tab_pos / t._tab_parent_height) * t._container_height });
        }

  };

  this.hideTab = function()
  {
    t._tab.hide();
  };

  this.showTab = function()
  {
    t._tab.show();
  };

  this._placeTab = function(new_pos)
  {
    var end = false;

    if(new_pos < 0)
      {
        new_pos = 0;
        end = true;
      }
      else if(new_pos > t._tab_parent_height)
        {
          new_pos = t._tab_parent_height;
          end = true;
        }

        t._tab.css({top: new_pos});

        return end;
  };

  this._mousemove = function(e)
  {
    if(!t._scroll)
      return true;

    var new_pos = t._last_tab_pos + (e.pageY - t._lastY);

    t._placeTab(new_pos);

    t._setScrollPos();

    e.stopImmediatePropagation();
    return false;
  };

  this._mousedown = function(e)
  {
    t._scroll = true;

    t.stopAutoscroll();

    t._last_tab_pos = t._tab.position().top;
    t._lastY = e.pageY;

    e.stopImmediatePropagation();
    return false;

  };

  this._mouseup = function(e)
  {
    t._scroll = false;
    //$(document).unbind('mousemove', this._mousemove);
  };

  this._mouseWheel = function(e)
  {
    var delta = -(e.wheelDelta/40) * 4;

    if (window.opera)
      delta = -delta;

    t._mouseScroll(delta);

    e.stopImmediatePropagation();
    return false;
  };

  this._mouseDOMWheel = function(e)
  {
    var delta = e.detail * 4;

    t._mouseScroll(delta);

    e.stopImmediatePropagation();
    return false;
  };

  this._mouseScroll = function(delta)
  {
    t.stopAutoscroll();

    var pos = t._tab.position().top + delta;

    t._placeTab(pos);

    t._setScrollPos();
  }

}
