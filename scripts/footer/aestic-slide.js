// aesticSlide
(function($) {

    // here we go!
    $.aesticSlide = function(element, options) {

        // PRIVATE PROPERTIES
        var currIndex = 0;
        var $currEl = null;
        var $currIn = null;
        var maxIndex = 0;
        var $infoList = null;
        var isMoving = false;
        var $navigation = null;

        var defaults = {
            effect: 'vertical', // "vertical", "horizontal" & "opacity"
            infoEffect: 'vertical', // "vertical", "horizontal" & "opacity"
            cssNameSpace: 'aestic-slide',
            duration: 1000,
            ease:'easeInOutQuart',
            infoContainer:'aestic-slide-info',
            navigationContainer:'aestic-slide-navigation',
            enableInfoShadow: true,
            heightOffset: 0,
            infoEffectDist: '30px',
            enableDirectionNav: true,
            enablePagingNav: false
        };

        var plugin = this;

        plugin.settings = {};

        var $element = $(element);

        plugin.$element = $element;

        plugin.init = function() {

            plugin.settings = $.extend({}, defaults, options);

            $(window).resize(resizeDimension($element));

            if($element.children().length > 1){
                $navigation = $('<div class="'+plugin.settings.navigationContainer+'"><a href="#" class="prev icon-left-open"></a><span class="thumbs"></span><a href="#" class="next icon-right-open"></a></div>').insertAfter($element);

                $navigation.find('.prev').click(function(e){
                    e.preventDefault();
                    plugin.prev();
                });
                $navigation.find('.next').click(function(e){
                    e.preventDefault();
                    plugin.next();
                });
                $element.children().each(function(i){
                    $('<a href="#">'+(i+1)+'</a>').appendTo($navigation.find('.thumbs'));
                });

                $navigation.find('.thumbs a').each(function(i){
                    $(this).click(function(e){
                        e.preventDefault();
                        plugin.goto(i);
                    });
                });
            }

            $infoList = $('<div class="'+plugin.settings.infoContainer+'"></div>').insertAfter($element);

            $element.find('li img').each(function(){
                var $img = $(this);
                var $wrap = $('<span class="caption"></span>');
                var $innerWrap = $('<span class="inner-wrap"></span>');
                var $siblings = $img.siblings();
                $siblings.appendTo($innerWrap);
                $innerWrap.appendTo($wrap);
                $wrap.appendTo($infoList);
            });

            maxIndex = $element.children().length-1;

            $currEl = $($element.children()[0]);
            $currIn = $($infoList.children()[0]);

            $currEl.css({ 'top':0, 'position':'absolute' });
            $currIn.css({ 'bottom':0, 'left':'0', 'right':'0', 'position':'absolute' });

            currIndex = 0;




            if($element.children().length > 1){
                $navigation.find('.thumbs').children().removeClass('active');
                $navigation.find('.thumbs').children('a:nth-child('+1+')').addClass('active');

                if(plugin.settings.enablePagingNav){
                    $navigation.find('.thumbs').show();
                }else{
                    $navigation.find('.thumbs').hide();
                }

                if(plugin.settings.enableDirectionNav){
                    $navigation.find('.prev,.next').show();
                }else{
                    $navigation.find('.prev,.next').hide();
                }
            }


            windowResizer();

        };


        // PUBLIC METHODS

        plugin.next = function(){
            if(currIndex==maxIndex){
                plugin.goto(0,'right');
            }else{
                plugin.goto(currIndex+1,'right');
            }
        };

        plugin.prev = function(){
            if(currIndex===0){
                plugin.goto(maxIndex,'left');
            }else{
                plugin.goto(currIndex-1,'left');
            }
        };


        plugin.getLength = function(){
            return maxIndex + 1;
        },

        plugin.getCurrentIndex =  function(){
            return currIndex;
        },


        plugin.goto = function(index, movement){

            var nextIndex = index;
            movement = (movement) ? movement : ( (nextIndex<currIndex) ? 'left' : 'right' );

            if(nextIndex!=currIndex && !isMoving){
                isMoving = true;

                var containerH = $element.css('padding-top');
                var containerW = $element.width();

                var $nextEl = $($element.children()[nextIndex]);
                var $nextIn = $($infoList.children()[nextIndex]);

                var nextInitStyle, nextAnimateSetting, currAnimateSetting;
                var nextInitStyleIn, nextAnimateSettingIn, currAnimateSettingIn;


                if(movement=='right'){

                    switch(plugin.settings.effect){

                        case 'vertical':
                            nextInitStyle       = {top:$currEl.height()*0.6+'px', position:'absolute', 'z-index':'-1' };
                            nextAnimateSetting  = {top:0+'px'};
                            currAnimateSetting  = {top:'-='+$currEl.height()};
                            break;

                        case 'horizontal':
                            nextInitStyle       = {top:0+'px', left:$currEl.width()*0.4+'px', position:'absolute', 'z-index':'-1'};
                            nextAnimateSetting  = {left:0+'px'};
                            currAnimateSetting  = {left:'-='+$currEl.width()};
                            break;

                        default:
                            nextInitStyle       = {top:0+'px', position:'absolute', 'z-index':'-1' };
                            nextAnimateSetting  = {};
                            currAnimateSetting  = {opacity:0};
                            break;

                    }
                    switch(plugin.settings.infoEffect){

                        case 'vertical':
                            nextInitStyleIn       = {left:'0px',right:'0px',bottom:'-'+plugin.settings.infoEffectDist, position:'absolute', opacity: 0 };
                            nextAnimateSettingIn  = {left:'0px',right:'0px',bottom:0+'px', opacity:1};
                            currAnimateSettingIn  = {left:'0px',right:'0px',bottom:plugin.settings.infoEffectDist,opacity:0};
                            break;

                        case 'horizontal':
                            nextInitStyleIn       = {bottom:'0px',left:plugin.settings.infoEffectDist,right:'-'+plugin.settings.infoEffectDist, position:'absolute', opacity: 0 };
                            nextAnimateSettingIn  = {bottom:'0px',left:'0px',right:'0px', opacity:1};
                            currAnimateSettingIn  = {bottom:'0px',left:'-'+plugin.settings.infoEffectDist,right:plugin.settings.infoEffectDist,opacity:0};
                            break;

                        default:
                            nextInitStyleIn       = {bottom:'0px', right:'0px', left:'0px',  position:'absolute', opacity: 0 };
                            nextAnimateSettingIn  = {opacity:1};
                            currAnimateSettingIn  = {opacity:0};
                            break;

                    }

                }else{



                    switch(plugin.settings.effect){

                        case 'vertical':
                            nextInitStyle       = {top:'-'+$nextEl.height()*0.4+'px', position:'absolute', 'z-index':'-1' };
                            nextAnimateSetting  = {top:0+'px'};
                            currAnimateSetting  = {top:'+='+$nextEl.height()+'px'};
                            break;

                        case 'horizontal':
                            nextInitStyle       = {top:0+'px', left:'-'+$nextEl.width()*0.6+'px', position:'absolute', 'z-index':'-1' };
                            nextAnimateSetting  = {left:0+'px'};
                            currAnimateSetting  = {left:$currEl.width()+'px'};
                            break;

                        default:
                            nextInitStyle       = {top:0+'px', position:'absolute', opacity: 0 };
                            nextAnimateSetting  = {opacity: 1};
                            currAnimateSetting  = {opacity:0};
                            break;

                    }
                    switch(plugin.settings.infoEffect){

                        case 'vertical':
                            nextInitStyleIn       = {left:'0px', right:'0px', bottom:plugin.settings.infoEffectDist, position:'absolute', opacity: 0 };
                            nextAnimateSettingIn  = {left:'0px', right:'0px', bottom:0+'px', opacity:1};
                            currAnimateSettingIn  = {left:'0px', right:'0px', bottom:'-'+plugin.settings.infoEffectDist,opacity:0};
                            break;

                        case 'horizontal':
                            nextInitStyleIn       = {bottom:'0px', left:'-'+plugin.settings.infoEffectDist, right:plugin.settings.infoEffectDist, position:'absolute', opacity: 0 };
                            nextAnimateSettingIn  = {bottom:'0px', left:'0px',right:'0px', opacity:1};
                            currAnimateSettingIn  = {bottom:'0px', left:plugin.settings.infoEffectDist, right:'-'+plugin.settings.infoEffectDist,opacity:0};
                            break;

                        default:
                            nextInitStyleIn       = {bottom:'0px', left:'0px', right:'0px', position:'absolute', opacity: 0 };
                            nextAnimateSettingIn  = {opacity:1};
                            currAnimateSettingIn  = {opacity:0};
                            break;
                    }


                }

                if($element.children().length > 1){
                    $nextIn.css(nextInitStyleIn);
                    $nextEl.css(nextInitStyle);

                    $nextIn.animate( nextAnimateSettingIn, plugin.settings.duration, plugin.settings.ease);
                    $nextEl.animate( nextAnimateSetting, plugin.settings.duration, plugin.settings.ease);

                    $currIn.animate( currAnimateSettingIn, plugin.settings.duration, plugin.settings.ease, function() {
                            $currIn.css({  'position':'', 'bottom':'', 'right':'', 'top':'', 'left':'', 'opacity':'' });
                            $currIn = $nextIn;
                        }
                    );
                    $currEl.animate( currAnimateSetting, plugin.settings.duration, plugin.settings.ease, function() {
                            $nextEl.css('z-index','0');
                            $currEl.css({ 'position':'', 'top':'', 'left':'', 'opacity':'' });
                            $currEl = $nextEl;
                            currIndex = nextIndex;
                            isMoving = false;
                            $navigation.find('.thumbs').children().removeClass('active');
                            $navigation.find('.thumbs').children('a:nth-child('+(currIndex+1)+')').addClass('active');
                        }
                    );
                }


            }

            $element.trigger({
                type: 'changeSlide',
                currentIndex: index
            });

        };

        plugin.refreshDimension = function(){
            resizeDimension($element)();
        };


        // PRIVATE METHODS

        var windowResizer = function(){

            if(document.readyState != 'complete' ){
                setTimeout(windowResizer, 10);
            }else{
                $(window).trigger('resize');
            }

        };

        var resizeDimension = function(el){

            return function(){

                var Ynav = plugin.settings.heightOffset;
                var Ymin = 0;
                var Xmin = 0;
                var Yc = $(window).height()-Ynav;
                var Xc = $(window).width();
                var Yr = null;
                var Xr = null;
                var infoContainer   = $element.siblings('.'+plugin.settings.infoContainer);
                var navContainer   = $element.siblings('.'+plugin.settings.navigationContainer);

                Yr = (Ymin>Yc) ? Ymin : Yc;
                Xr = (Xmin>Xc) ? Xmin : Xc;

                $(el).css('padding-top',Yr);
                infoContainer.css('padding-top',Yr);

                infoContainer.children().each(function(i,e){
                    var innerWrap = $(e).children('.inner-wrap');
                    $(e).height(Yr);
                    innerWrap.css('margin-top',innerWrap.height()/-2);
                });


                $(el).children().each(function(index){

                    var slide           = $(this);
                    var state           = slide.data('state');
                    var image           = slide.children('img');
                    image.removeAttr('width');
                    image.removeAttr('height');
                    image.css({
                        'width':'',
                        'height':''
                    });
                    var Xi              = image.width();
                    var Yi              = image.height();

                    slide.attr('index_elm', index);

                    slide.width(Xr);
                    slide.height(Yr);

                    if((Xi/Yi)<(Xr/Yr)){
                        image.css('width',Xr+'px');
                        image.css('height','');
                        image.css({ 'margin-left':'' });
                        image.css({ 'margin-top': '-'+Math.abs((Yr - (Xr/Xi)*Yi)/2)+'px' });
                    }else{
                        image.css('height',Yr+'px');
                        image.css('width','');
                        image.css({ 'margin-top':''});
                        image.css({'margin-left': '-'+Math.abs((Xr - (Yr/Yi)*Xi)/2)+'px' });
                    }


                });

            };
        };

        plugin.init();

    };

    $.fn.aesticSlide = function(options) {
        return this.each(function() {
            if (undefined === $(this).data('aesticSlide')) {
                var plugin = new $.aesticSlide(this, options);
                $(this).data('aesticSlide', plugin);
            }
        });
    };

})(jQuery);


