AE.header = Backbone.View.extend({

    home: null,

    el: 'header',

    expanded: false,

    isHideFromPortfolioSingle: false,


    initialize: function(){
        this.maintainDimension();
    },

    hideFromPortfolioSingle: function(silent, callback, callbackThis, args, direction){
        if(!this.isHideFromPortfolioSingle){
            var winHeight   = $(window).height();
            var navHeight   = this.$('.nav-wrap').height();
            if(silent){
                this.$el.css({ 'top':'-'+winHeight+'px' });
                if(callback){
                    callback.call(callbackThis);
                }
            }else{
                this.$el.animate({ 'top':'-'+winHeight+'px' }, AE.opt.duration, AE.opt.easing, function(){
                    if(callback){
                        callback.call(callbackThis,true);
                    }
                });
            }
            this.isHideFromPortfolioSingle = true;
            this.trigger('hide');
        }
    },

    expand: function(silent, callback, callbackThis, args, direction){
        if(!this.expanded || this.isHideFromPortfolioSingle){
            var winHeight   = $(window).height();
            var navHeight   = this.$('.nav-wrap').height();
            if(silent){
                this.$el.css({ 'top':'0px' });
                if(callback){
                    callback.call(callbackThis);
                }
            }else{
                this.$el.animate({ 'top':'0px' }, AE.opt.duration, AE.opt.easing, function(){
                    if(callback){
                        callback.call(callbackThis, true);
                    }
                });
            }
            this.expanded = true;
            this.isHideFromPortfolioSingle = false;
            this.trigger('show');
        }
    },

    collapse: function(silent, callback, callbackThis, args, direction){
        if(this.expanded || this.isHideFromPortfolioSingle){
            var winHeight   = $(window).height();
            var navHeight   = this.$('.nav-wrap').height();
            var dif = winHeight - navHeight;
            if(silent){
                this.$el.css({ 'top':'-'+dif+'px' });
                if(callback){
                    callback.call(callbackThis);
                }
            }else{
                this.$el.animate({ 'top':'-'+dif+'px' }, AE.opt.duration, AE.opt.easing, function(){
                    if(callback){
                        callback.call(callbackThis,true);
                    }
                });
            }
            this.expanded = false;
            this.isHideFromPortfolioSingle = false;
            this.trigger('hide');
        }
    },


    maintainDimension: function(){
        var winHeight   = $(window).height(),
        navHeight   = this.$('.nav-wrap').height(),
        dif         = winHeight - navHeight;

        this.$el.css('height',winHeight+'px');

        if(this.isHideFromPortfolioSingle){
            this.$el.css('top','-'+winHeight+'px');
        }else{
            if(this.expanded){
                this.$el.css('top','0px');
            }else{
                this.$el.css('top','-'+dif+'px');
            }
        }
    },

    setNavActive: function(hash){
        var isFirst = false;
        if($('nav .pointer').length===0){
            isFirst = true;
            $('<div class="pointer" style="position:absolute;opacity:0;"></div>').appendTo('nav');
        }
        var navs = $('nav a');
        navs.each(function(index,el){
            var hashHref = $(el).attr('href').replace(/#\//g,'');
                if(hash.split('/')[0]==hashHref){
                $(el).addClass('current');
            }else{
                $(el).removeClass('current');
            }
        });

        var target          = $('nav a.current');
        var targetMargin    = parseInt(target.css('padding-left')) +parseInt(target.css('margin-left'));
        var pos             = target.position();
        var pointer         = $('nav .pointer');
        var speed           = isFirst ? 0 : 400;
        var ease            = 'easeOutBack';
        pointer.animate({
            opacity:1,
            top: pos.top+(target.height()/2)+10,
            left: pos.left+targetMargin,
            width: target.width()
        }, speed, ease, function(){
            var target          = $('nav a.current');
            var targetMargin    = parseInt(target.css('padding-left'))+parseInt(target.css('margin-left'));
            var pos             = target.position();
            var pointer         = $(this);

            pointer.animate({
                top: pos.top+(target.height()/2)+10,
                left: pos.left+targetMargin,
                width: target.width()
            }, speed/2, ease);
        });
    }



});

