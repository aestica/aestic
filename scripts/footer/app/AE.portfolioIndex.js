AE.portfolioIndex = AE.baseView.extend({

    initialize: function(){
        this.render();
        this.identify();
        this.maintainDimension();
    },

    maintainDimension: function(){

        var heightOffset= $('.nav-wrap').height();
        var newWidth    = $(window).width();
        var newHeight   = $(window).height()-heightOffset;
        this.$el.css({
            'top':heightOffset,
            'width':newWidth,
            'height':newHeight
        });
        this.$el.find('.layer').css({
            'min-height':newHeight
        });
        this.$el.find('.fit-window').css({
            'width':newWidth,
            'height':newHeight
        });



        var offset = this.$el.hasClass('no-category') ? 0 : 220;
        //var winWidth = $(window).width() - 0 - offset - 60;
        var winWidth = $(window).width();
        var horzCount= Math.ceil(winWidth/AE.opt.portfolioThumbWidth);
        var porthumb = (winWidth/horzCount) - 0;
        var porthumbH = porthumb * AE.opt.portfolioHeightRatio;

        $(this.el).find('.wrap ul li a').css({
            'height': Math.round(porthumbH),
            'width' : porthumb
        });

        $(this.el).find('img.bg-cover').each(AE.fitImgWithWindow);
    },

    parseParams: function(params, silent){

        this.$('.categories li.active').removeClass('active');
        if(params.category){
            this.$('ul.portfolio-list > li.'+params.category)
            //.animate({opacity:1})
            .removeClass('inactive');
            this.$('ul.portfolio-list > li').not('.'+params.category)
            //.animate({opacity:0.2})
            .addClass('inactive');
            this.$('.categories li.'+params.category).addClass('active');
        }else{
            this.$('ul.portfolio-list > li')
            //.animate({opacity:1})
            .removeClass('inactive');
            this.$('.categories li.all').addClass('active');
        }

    },

    beforeCollapse: function(){
        this.$('.categories').hide();
    },

    afterExpand: function(){
        this.$('.categories').show();
    }


});

