AE.home = AE.baseView.extend({

    header: null,
    slider: null,

    initialize: function(){
        this.header = this.options.header;
        this.identify();
        this.render();
        this.maintainDimension();

        this.header.on('show', this.enableKeyEvent, this);
        this.header.on('hide', this.disableKeyEvent, this);
    },

    render: function(){
        this.$el.insertBefore('.nav-wrap');
        this.$('ul.aestic-slide-images').aesticSlide({
            infoEffect:'vertical',
            effect:'horizontal',
            duration: AE.opt.duration,
            ease: AE.opt.easing,
            heightOffset: $('.nav-wrap').height(),
            enablePagingNav: AE.opt.showSlideIndexHome,
            enableDirectionNav: true
        });
        this.slider = this.$('ul.aestic-slide-images').data('aesticSlide');
        return this;
    },

    expand: function(silent, callback, callbackThis, args, direction){
        this.header.expand( silent, callback, callbackThis, args, direction );
    },

    collapse: function(silent, callback, callbackThis, args, direction){
        this.header.collapse( silent, callback, callbackThis, args, direction );
    },

    enableKeyEvent: function(){
        $(document).on('keydown.'+this.cid,this.keydownCallback.call(this));
    },

    disableKeyEvent: function(){
        $(document).off('keydown.'+this.cid);
    },

    keydownCallback: function(){
        var thisObj = this;
        return function(e){
            console.log(e.keyCode);
            switch(e.keyCode){
                case 37 :
                    case 38 :
                    thisObj.slider.prev();
                break;
                case 39 :
                    case 40 :
                    thisObj.slider.next();
                break;
            }
        };
    },

    maintainDimension: function(){
        this.$el.find('img.bg-cover').each(AE.fitImgWithWindow);
    }


});

