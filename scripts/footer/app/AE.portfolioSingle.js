AE.portfolioSingle = AE.baseView.extend({

    slider: null,

    events: {
        "click span.toggle-details": "toggleDetails",
        "click a.close": "closeView"
    },

    render: function(){
        this.$el.insertBefore('.preloader');
        //this.$('.flexslider').flexslider({
        //animation: "slide",
        //easing: AE.opt.easing,
        //animationSpeed: AE.opt.duration,
        //smoothHeight: true
        //});
        this.$('ul.aestic-slide-images').aesticSlide({
            infoEffect:'vertical',
            effect:'horizontal',
            duration: AE.opt.duration,
            ease: AE.opt.easing,
            enableInfo: false,
            enableDirectionNav: true,
            enablePagingNav: false,
            enableInfoShadow: false
        });
        this.slider = this.$('ul.aestic-slide-images').data('aesticSlide');

        this.slider.$element.on('changeSlide', function(e){
            console.log(e.currentIndex);
        });

        if(AE.opt.showPortfolioInfoInitially){
            this.showDetails();
        }else{
            this.hideDetails();
        }

        this.initScroller();

        this.refreshContentDimension()();

        jQuery(window).on('resize', this.refreshContentDimension.apply(this) );


        return this;
    },


    refreshContentDimension: function(){
        return function(){
            var $content = this.$('.portfolio-desc div.content');
            var winHeight = jQuery(window).height();
            var titleHeight = this.$('.portfolio-desc h1.title').height() + parseInt(this.$('h1.title').css('margin-top')) + parseInt(this.$('h1.title').css('margin-bottom'));
            var toggleHeight = this.$('.portfolio-desc .toggle-details').height();
            var offset = 150;

            var contentHeight = winHeight - (titleHeight + toggleHeight + offset);

            $content.height(contentHeight);
        };
    },

    showDetails: function(){
        var $parent = this.$el;
        if(!$parent.hasClass('active')){
            this.showScrolls();
            $parent.addClass('active');
        }
    },

    hideDetails: function(){
        var $parent = this.$el;
        if($parent.hasClass('active')){
            this.hideScrolls();
            $parent.removeClass('active');
        }
    },

    toggleDetails: function(){
        var $parent = this.$el;
        if($parent.hasClass('active')){
            this.hideScrolls();
            $parent.removeClass('active');
        }else{
            this.showScrolls();
            $parent.addClass('active');
        }
    },

    expand: function(silent, callback, callbackThis, args, direction){
        if(!this.expanded){
            this.$el.css({'z-index':1});
            if(callback) callback.call(callbackThis, false, null, null, null, 'fromPortfolioSingle');
            this.$el.show();
            this.expanded = true;
            this.maintainDimension();
            this.enableKeyEvent();
            if(AE.opt.showPortfolioInfoInitially){
                this.showDetails();
            }else{
                this.hideDetails();
            }
            this.refreshContentDimension()();
        }
    },


    collapse: function(silent, callback, callbackThis, args, direction){
        this.hideScrolls();
        if(this.expanded){
            if(direction == 'fromPortfolioSingle'){
                var thisObj = this;
                this.$el.css({ 'z-index':2});
                this.$el.animate({'top':'200px','opacity':0}, AE.opt.duration, AE.opt.easing,function(){
                    if(callback) callback.call(callbackThis);
                    thisObj.expanded = false;
                    thisObj.disableKeyEvent();
                    thisObj.$el.css({ 'z-index':1, 'top':'', 'opacity':''});
                    thisObj.$el.hide();
                    thisObj.hideDetails();
                });
            }else{
                this.$el.css({ 'z-index':1});
                this.$el.hide();
                if(callback) callback.call(callbackThis);
                this.expanded = false;
                this.disableKeyEvent();
                this.hideDetails();
            }
        }
    },

    enableKeyEvent: function(){
        $(document).on('keyup.'+this.cid,this.keydownCallback.call(this));
    },

    disableKeyEvent: function(){
        $(document).off('keyup.'+this.cid);
    },

    keydownCallback: function(){
        var thisObj = this;
        return function(e){
            console.log(e.keyCode);
            switch(e.keyCode){
                case 37 :
                case 38 :
                    thisObj.slider.prev();
                    break;
                case 39 :
                case 40 :
                    thisObj.slider.next();
                    break;
                case 27 :
                    var $parent = thisObj.$('.portfolio-desc');
                    if($parent.hasClass('active')){
                        $parent.removeClass('active');
                    }else{
                        window.location.assign("#/portfolio");
                    }
            }
        };
    },

    maintainDimension: function(){
        var newWidth    = $(window).width();
        var newHeight   = $(window).height();
        this.$el.css({
            'top':0,
            'width':newWidth,
            'height':newHeight
        });
        this.$el.find('.layer').css({
            'min-height':newHeight
        });
        this.$el.find('.fit-window').css({
            'width':newWidth,
            'height':newHeight
        });
        this.$el.find('img.bg-cover').each(AE.fitImgWithWindow);
        this.slider.refreshDimension();
    },

    closeView: function(e){
        e.preventDefault();
        if(Aestic.previous === null){
            window.location.assign('#/portfolio');
        }else{
            history.back();
        }
    }


});

