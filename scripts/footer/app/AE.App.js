AE.App = Backbone.Router.extend({


    jqXHR: null,


    current: null,
    previous: null,


    views: {},


    header: null,


    routes: {
        '':                             'home',
        'home':                         'home',
        'portfolio':                    'portfolioIndex',
        'portfolio/category::category': 'portfolioIndex',
        'portfolio/:id':                'portfolioSingle',
        'blog':                         'blogIndex',
        'blog/:id':                     'blogSingle',
        'contact':                      'contact',
        '*path':                        'staticPage'
    },


    initialize: function(){
        if(Modernizr.touch){
            $('body').addClass('touch-device');
        }else{
            $('body').addClass('no-touch-device');
        }
        this.showPreloader('initial loading', true);
        this.header = new AE.header();
        $(window).resize(this.resizeCallback(this));
    },


    resizeCallback: function(thisObj){
        return function(){
            var keys = _.keys(thisObj.views);
            for(var i=0; i<keys.length; i++){
                thisObj.views[keys[i]].maintainDimension();
            }
            thisObj.header.maintainDimension();
        };
    },



    checkPresence: function(path){

        return this.views[path] !== undefined;

    },


    showPreloader: function(string, silent, opacity){
        string = !string ? '' : string;
        $('.preloader > div.text').html(string);
        if(silent){
            $('.preloader').show();
        }else{
            $('.preloader').fadeIn('fast');
        }
    },


    hidePreloader: function(){
        $('.preloader').fadeOut(AE.opt.duration, function(){
            $(this).removeClass('initial');
        });
    },



    load: function(path, param, callback){

        if(this.jqXHR !== null){
            this.jqXHR.abort();
            this.jqXHR = null;
        }

        if(this.checkPresence(path)){

            this.displayContent(this, path, param)();

        }else{

            this.showPreloader('Loading ...', false, 0.5);

            var url = AE.opt.pageBaseUrl + path + AE.opt.pagePostfix;

            this.jqXHR = $.ajax(url, {
                complete: this.loadComplete(this, path, param)
            });

        }

    },



    loadComplete: function(thisObj, path, param){

        return function(data, status){

            switch(status){

                case 'success':
                    var $html = $(data.responseText);
                    $html.allImageLoaded(thisObj.onAllImageLoaded(thisObj, path, param, $html));
                    break;


                case 'notmodified':
                    thisObj.hidePreloader();
                    thisObj.jqXHR = null;
                    alert('notmodified');
                    break;


                case 'error':
                    thisObj.hidePreloader();
                    thisObj.jqXHR = null;
                    alert('error');
                    break;


                case 'timeout':
                    thisObj.hidePreloader();
                    thisObj.jqXHR = null;
                    alert('timeout');
                    break;


                case 'abort':
                    thisObj.hidePreloader();
                    thisObj.jqXHR = null;
                    alert('aborted');
                    break;


                case 'parsererror':
                    thisObj.hidePreloader();
                    thisObj.jqXHR = null;
                    alert('parsererror');
                    break;


            }


        };

    },


    onAllImageLoaded: function(thisObj, path, param, $html){

        return function(){

            var type = $html.data().type;

            if(type == 'home'){
                thisObj.views[path] =  new AE[type]({el:$html, header: thisObj.header});
            }else{
                thisObj.views[path] =  new AE[type]({el:$html});
            }

            thisObj.displayContent(thisObj, path, param)();

        };

    },


    displayContent: function(thisObj, path, param){

        return function(){

            if(thisObj.current !== null){

                switch(thisObj.current.type){

                    case 'home':
                        if(thisObj.views[path].type == 'portfolioSingle'){
                            thisObj.views[path].expand();
                            thisObj.header.hideFromPortfolioSingle();
                        }else{
                            thisObj.views[path].expand(true);
                            thisObj.current.collapse();
                        }
                        break;

                    case 'portfolioSingle':
                        thisObj.current.hideScrolls();
                        if(thisObj.views[path].type == 'home'){
                            thisObj.views[path].expand(false, thisObj.current.collapse, thisObj.current, null);
                        }else if(thisObj.views[path].type == 'portfolioSingle'){
                            thisObj.views[path].expand(false, thisObj.current.collapse, thisObj.current, null, 'fromPortfolioSingle');
                        }else{
                            thisObj.header.collapse(false, null, null, null, 'toEdge');
                            thisObj.views[path].expand(false, thisObj.current.collapse, thisObj.current, null, 'fromPortfolioSingle');
                        }
                        break;

                    //case 'blogIndex':
                        //if(thisObj.views[path].type == 'blogSingle'){
                            //thisObj.current.collapse(false, null, null, null, 'left');
                            //thisObj.views[path].expand(false, null, null, null, 'left');
                        //}
                        //break;

                    default:
                        if(thisObj.views[path].type == 'home'){
                            thisObj.views[path].expand(false, thisObj.current.collapse, thisObj.current);
                        }else if(thisObj.views[path].type == 'portfolioSingle'){
                            thisObj.views[path].expand();
                            thisObj.current.collapse(false, null, null, null, 'toPortfolioSingle');
                            thisObj.header.hideFromPortfolioSingle();
                        }else if(thisObj.views[path].type == 'blogSingle' && thisObj.current.type == 'blogIndex'){
                            thisObj.current.collapse(false, null, null, null, 'left');
                            thisObj.views[path].expand(false, null, null, null, 'left');
                        }else{
                            var direction = thisObj.current.index < thisObj.views[path].index ? 'left' : 'right';
                            thisObj.current.collapse(false, null, null, null, direction);
                            thisObj.views[path].expand(false, null, null, null, direction);
                        }
                        break;
                }


            }else{
                if(thisObj.views[path].type == 'portfolioSingle'){
                    thisObj.views[path].expand(true);
                    thisObj.header.hideFromPortfolioSingle(true);
                }else{
                    thisObj.views[path].expand(true);
                }
            }


            thisObj.previous = thisObj.current;
            thisObj.current = thisObj.views[path];
            thisObj.header.setNavActive(path);

            if(param){
                thisObj.current.parseParams(param, true);
            }

            thisObj.hidePreloader();
            thisObj.jqXHR = null;

        };

    },


    home: function(){
        if(this.current){
            if( this.current.type != 'home'){
                this.load('home');
            }
        }else{
            this.load('home');
        }
    },


    portfolioIndex: function(cat){
        if(this.current){
            if( this.current.type != 'portfolioIndex'){
                this.load('portfolio', {category: cat});
            }else{
                this.current.parseParams({category:cat});
            }
        }else{
            this.load('portfolio', {category: cat});
        }
    },


    portfolioSingle: function(id){
        this.load('portfolio/'+id);
    },


    blogIndex: function(cat){
        this.load('blog', {category: cat});
    },


    blogSingle: function(id){
        this.load('blog/'+id);
    },


    staticPage: function(path){
        this.load(path);
    },


    contact: function(){
        this.load('contact');
    }


});


