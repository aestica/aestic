AE.baseView = Backbone.View.extend({

    index: null,
    type: null,


    expanded: false,


    initialize: function(){
        this.render();
        this.identify();
        this.maintainDimension();
    },


    identify: function(){
        this.index = this.$el.data('index');
        this.type = this.$el.data('type');
    },


    render: function(){
        this.$el.insertBefore('.preloader');
        this.initScroller();
        return this;
    },


    initScroller: function(){

        var scrollOpt = {
            cursoropacitymin: 0.4,
            cursorborder: 'none',
            zindex: 2,
            //autohidemode: false,
            cursorwidth: 8,
            railoffset:{left:-6}
        };

        if(this.$el.hasClass('scrollable')){
            this.$el.niceScroll(scrollOpt);
            this.$el.getNiceScroll()[0].rail.addClass('rail');
        }

        this.$('.scrollable').niceScroll(scrollOpt);
        this.$('.scrollable').each(function(){
            jQuery(arguments[1]).getNiceScroll()[0].rail.addClass('rail');
        });

    },


    expand: function(silent, callback, callbackThis, args, direction){
        this.hideScrolls();
        this.beforeExpand();
        var heightOffset= $('.nav-wrap').height();
        var thisObj = this;
        if(!this.expanded){
            this.$el.show();
            this.$el.css({'z-index':3});
            if(silent){
                this.$el.css({ 'left':'0px' });
                this.$el.css({ 'top':heightOffset+'px' });
                if(callback) callback.call(callbackThis);
                this.showScrolls();
                this.afterExpand();
            }else{
                if(direction=='fromPortfolioSingle'){
                    var winHeight = $(window).height();
                    var initTop = winHeight;
                    this.$el.css('left', '0px');
                    this.$el.css('top', initTop+'px');
                    this.$el.animate({ 'top':heightOffset+'px' }, AE.opt.duration, AE.opt.easing, function(){
                        if(callback) callback.call(callbackThis);
                        thisObj.showScrolls();
                        thisObj.afterExpand();
                    });
                }else{
                    var winWidth = $(window).width();
                    var initLeft = direction=='left' ? winWidth : '-'+winWidth;
                    this.$el.css('top',heightOffset+'px');
                    this.$el.css('left',initLeft+'px');
                    this.$el.animate({ 'left':'0px' }, AE.opt.duration, AE.opt.easing, function(){
                        if(callback) callback.call(callbackThis);
                        thisObj.showScrolls();
                        thisObj.afterExpand();
                    });
                }
            }
            this.expanded = true;
        }
    },

    beforeExpand: function(){ },

    afterExpand: function(){ },

    collapse: function(silent, callback, callbackThis, args, direction){
        this.beforeCollapse();
        this.hideScrolls();
        var thisObj  = this;
        if(this.expanded){
            this.$el.css({ 'z-index':2});
            if(silent){
                if(callback) callback.call(callbackThis);
                this.resetScrolls();
                this.$el.hide();
                this.afterCollapse();
            }else{
                if(direction=='toPortfolioSingle'){
                    var winHeight = $(window).height();
                    var postTop = winHeight;
                    this.$el.animate({ 'top':postTop+'px' }, AE.opt.duration, AE.opt.easing, function(){
                        if(callback) callback.call(callbackThis);
                        thisObj.resetScrolls();
                        $(thisObj.el).hide();
                        thisObj.afterCollapse();
                    });
                }else{
                    var winWidth = $(window).width();
                    var postLeft = direction=='left' ? '-'+winWidth : winWidth;
                    this.$el.animate({ 'left':postLeft+'px' }, AE.opt.duration, AE.opt.easing, function(){
                        if(callback) callback.call(callbackThis);
                        thisObj.resetScrolls();
                        $(thisObj.el).hide();
                        thisObj.afterCollapse();
                    });
                }
            }
            this.expanded = false;
        }
    },

    beforeCollapse: function(){ },

    afterCollapse: function(){ },

    hideScrolls: function(){

        if(this.$el.hasClass('scrollable')){
            this.$el.getNiceScroll()[0].hide();
        }

        this.$('.scrollable').each(function(){
            jQuery(arguments[1]).getNiceScroll()[0].hide();
        });

    },


    showScrolls: function(){

        if(this.$el.hasClass('scrollable')){
            this.$el.getNiceScroll()[0].show();
        }

        this.$('.scrollable').each(function(){
            if(jQuery(arguments[1]).getNiceScroll().length){
                jQuery(arguments[1]).getNiceScroll()[0].show();
            }
        });

    },


    resetScrolls: function(){

        if(this.$el.hasClass('scrollable')){
            this.$el.getNiceScroll()[0].scrollTop(0);
        }

        this.$('.scrollable').each(function(){
            jQuery(arguments[1]).getNiceScroll()[0].scrollTop(0);
        });

    },


    maintainDimension: function(){
        var heightOffset= $('.nav-wrap').height();
        var newWidth    = $(window).width();
        var newHeight   = $(window).height()-heightOffset;
        this.$el.css({
            'top':heightOffset,
            'width':newWidth,
            'height':newHeight
        });
        this.$el.find('.layer').css({
            'min-height':newHeight
        });
        this.$el.find('.fit-window').css({
            'width':newWidth,
            'height':newHeight
        });
        this.$el.find('img.bg-cover').each(AE.fitImgWithWindow);
    },


    parseParams: function(params, silent){ }


});

