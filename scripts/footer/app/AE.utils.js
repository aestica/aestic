var AE = {};

var loc = window.location;
var baseUrl = loc.protocol+'//'+loc.host+loc.pathname;

AE.opt = {
    duration                    : 800,
    easing                      : 'easeInOutCirc',
    portfolioThumbWidth         : 400,
    portfolioHeightRatio        : 0.8,
    pageBaseUrl                 : baseUrl+'pages/',
    pagePostfix                 : '.html',
    showSlideIndexHome          : false,
    showPortfolioInfoInitially  : true
};


AE.fitImgWithWindow = function(index, img){

    var image = $(img);

    var Xr = $(window).width();
    var Yr = $(window).height();

    image.removeAttr('width').removeAttr('height');

    var Xi = image.width();
    var Yi = image.height();

    var finalH, finalW;

    image.css({'margin-top':'', 'margin-left':''});
    if((Xr/Yr)>(Xi/Yi)){

        //console.log('b');

        finalW = Xr;
        finalH = (Xr/Xi)*Yi;

        image.attr('width',  finalW+"px");
        image.attr('height', finalH+"px");

        var delH = (Yr - finalH) / 2;
        image.css('margin-top', delH + 'px');

    }else{
        //console.log('a');

        finalH = Yr;
        finalW = (Yr/Yi)*Xi;

        image.attr('height', finalH+"px");
        image.attr('width',  finalW+"px");

        var delW = (Xr - finalW) / 2;
        image.css('margin-left', delW + 'px');

    }

};

