;(function ( $, window, undefined ) {

    var pluginName = 'allImageLoaded',
    document = window.document,
    $images = null,
    imageTotal = 0,
    imageLoaded = 0,
    self = null,
    defaults = {};

    function Plugin( element, options ) {
        this.element = $(element);
        self = this;
        if(typeof(options) === 'function'){
            self.callback = options;
        }
        self.options = $.extend( {}, defaults, options) ;
        self._defaults = defaults;
        self._name = pluginName;
        self.init();
    }

    function imgOnLoad(){
        imageLoaded++;
        if(imageLoaded == imageTotal){
            self.callback();
        }
    }

    function imgOnError(){
        imageLoaded++;
        if(imageLoaded == imageTotal){
            self.callback();
        }
    }

    Plugin.prototype.init = function () {

        $images = self.element.find('img');
        imageTotal = $images.length;
        imageLoaded = 0;

        if(imageTotal > 0){
            $images.each(function(index, item){
                $(item).load(imgOnLoad).bind('error', imgOnError);
            });
        }else{
            self.callback();
        }


    };

    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
            }
        });
    };

}(jQuery, window));
