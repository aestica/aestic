LESS_DIR = ./styles/css/less
CSS_DIR = ./styles/css
SOURCES = `ls $(LESS_DIR)/style-*.less`

build:
	@for i in $(SOURCES); do \
		S=`basename $$i .less`; \
		lessc $(LESS_DIR)/$$S.less $(CSS_DIR)/$$S.css ; \
	done;
